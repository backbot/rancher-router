#!/usr/bin/env ruby
#require 'rubygems'
require 'logger'
require 'open3'
require 'fileutils'
require_relative 'render_context'

class RancherRouter

  attr_reader :url, :rancher_port, :domain, :nginx_port, :email

  def initialize(url: nil, rancher_port: nil, domain: nil, nginx_port: nil, email: nil, service_conf_path: nil)
    @log = Logger.new(STDERR)

    @url = url || ENV['RR_URL'] || 'rancher'
    @rancher_port = rancher_port || ENV['RR_PORT'] || 8080
    @domain = domain || ENV['RR_DOMAIN']
    @nginx_port = nginx_port || ENV['RR_NGINX_PORT'] || 8080
    @email = email || ENV['RR_EMAIL']
    check_variables

    @service_conf_path = service_conf_path || '/etc/nginx/services/rancher.conf'
    configure_nginx
  end

  def ssl_certificate
    if File.exist? cert_path
      cert_path
    else
      '/etc/nginx/nginx.crt'
    end
  end

  def ssl_certificate_key
    real_key = "/etc/letsencrypt/live/#{@domain}/privkey.pem"
    if File.exist? real_key
      real_key
    else
      '/etc/nginx/nginx.key'
    end
  end

  private
  def configure_nginx
    FileUtils.mkpath(File.dirname(@service_conf_path))
    File.open(@service_conf_path, 'w') { |f| f.puts service_conf }
    reload_nginx
    certify_domain
    File.open(@service_conf_path, 'w') { |f| f.puts service_conf } # config again to change path of certificate.
    reload_nginx
  end

  def service_conf
    RenderContext.new(self).render(File.read('app/templates/services.conf.erb'))
  end

  def certify_domain
    unless valid_cert?
      system("/letsencrypt/letsencrypt-auto certonly --webroot -w /tmp/letsencrypt -d #{@domain} --email #{@email} --text --agree-tos")
      cert = File.read(cert_path)
      File.write(cert_path, cert + ca) # add ca to cert
      system("cp #{cert_path} /ssl/ca.crt") if File.exist? cert_path
    end
  rescue => e
    @log.error("Nginx: Error creating cert. #{e}")
  end

  def reload_nginx
    @log.info("Nginx: Reloading.")
    o, error, s = Open3.capture3(reload_command)
    unless error.include? 'signal process started'
      @log.warn("Nginx: Error while reloading. Message: #{error}")
      false
    else
      @log.info("Nginx: #{error}")
      true
    end
  end

  def reload_command
    "#{bin} -s reload"
  end

  def bin
    o, error, s = Open3.capture3('which nginx')
    if error != ''
      @log.warn("Nginx: Error trying to find command. Use 'nginx' for now. Message: #{error}")
      'nginx'
    else
      o.gsub("\n", '')
    end
  end

  def valid_cert?
    if File.exist?(cert_path)
      certificate = OpenSSL::X509::Certificate.new(File.read(cert_path))

      days_valid = (certificate.not_after - Time.now)
      if days_valid.to_i > 30
        @log.info("Certs: Certificate #{cert_path} is valid more than 30 days. Days valid: #{(days_valid/3600/24).to_i}")
        true
      else
        false
      end
    else
      @log.info("Certs: No cert yet for #{cert_path}")
      false
    end
  rescue => e
    @log.error "Certs: not valid cert for path: #{cert_path}. Exception: #{e}"
    false
  end

  def cert_path
    "/etc/letsencrypt/live/#{@domain}/fullchain.pem"
  end

  def check_variables
    raise "ERROR: Please define 'RR_DOMAIN'!" unless @domain
    raise "ERROR: Please define 'RR_EMAIL'!" unless @email
  end

  def ca
    "-----BEGIN CERTIFICATE-----
MIIDSjCCAjKgAwIBAgIQRK+wgNajJ7qJMDmGLvhAazANBgkqhkiG9w0BAQUFADA/
MSQwIgYDVQQKExtEaWdpdGFsIFNpZ25hdHVyZSBUcnVzdCBDby4xFzAVBgNVBAMT
DkRTVCBSb290IENBIFgzMB4XDTAwMDkzMDIxMTIxOVoXDTIxMDkzMDE0MDExNVow
PzEkMCIGA1UEChMbRGlnaXRhbCBTaWduYXR1cmUgVHJ1c3QgQ28uMRcwFQYDVQQD
Ew5EU1QgUm9vdCBDQSBYMzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEB
AN+v6ZdQCINXtMxiZfaQguzH0yxrMMpb7NnDfcdAwRgUi+DoM3ZJKuM/IUmTrE4O
rz5Iy2Xu/NMhD2XSKtkyj4zl93ewEnu1lcCJo6m67XMuegwGMoOifooUMM0RoOEq
OLl5CjH9UL2AZd+3UWODyOKIYepLYYHsUmu5ouJLGiifSKOeDNoJjj4XLh7dIN9b
xiqKqy69cK3FCxolkHRyxXtqqzTWMIn/5WgTe1QLyNau7Fqckh49ZLOMxt+/yUFw
7BZy1SbsOFU5Q9D8/RhcQPGX69Wam40dutolucbY38EVAjqr2m7xPi71XAicPNaD
aeQQmxkqtilX4+U9m5/wAl0CAwEAAaNCMEAwDwYDVR0TAQH/BAUwAwEB/zAOBgNV
HQ8BAf8EBAMCAQYwHQYDVR0OBBYEFMSnsaR7LHH62+FLkHX/xBVghYkQMA0GCSqG
SIb3DQEBBQUAA4IBAQCjGiybFwBcqR7uKGY3Or+Dxz9LwwmglSBd49lZRNI+DT69
ikugdB/OEIKcdBodfpga3csTS7MgROSR6cz8faXbauX+5v3gTt23ADq1cEmv8uXr
AvHRAosZy5Q6XkjEGB5YGV8eAlrwDPGxrancWYaLbumR9YbK+rlmM6pZW87ipxZz
R8srzJmwN0jP41ZL9c8PDHIyh8bwRLtTcm1D9SZImlJnt1ir/md2cXjbDaJWFBM5
JDGFoqgCWjBH4d1QB7wCCZAA62RjYJsWvIjJEubSfZGL+T0yjWW06XyxV3bqxbYo
Ob8VZRzI9neWagqNdwvYkQsEjgfbKbYK7p2CNTUQ
-----END CERTIFICATE-----"
  end

end
